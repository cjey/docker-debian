FROM debian:jessie

ADD debian/etc/apt/ /etc/apt/

RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y sudo vim curl tree iftop iotop nmap tcpdump arping netcat tmux bvi unzip unrar-free dnsutils git colordiff locales bash-completion man net-tools nano wget file lsb-release openssh-server
RUN apt-get install -y apt-utils dialog

ADD debian/ /

RUN cp /usr/share/zoneinfo/PRC /etc/localtime
RUN locale-gen

# should be inherited
#RUN rm /etc/ssh/ssh_host_*
#RUN dpkg-reconfigure openssh-server

RUN useradd -s /bin/bash -m -r paadoo
RUN usermod -aG sudo paadoo
RUN echo 'paadoo:paadoo' | chpasswd
RUN passwd -e paadoo

RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
RUN mkdir -p /var/run/sshd
EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]
